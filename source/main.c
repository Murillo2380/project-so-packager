/*! \mainpage My Personal Index Page
 *
 * \section intro_sec Introdução
 *
 * Este é um trabalho para a disciplina de Sistemas Operacionais, na qual consiste em um empacotador de arquivos sem o uso de compressão
 *
 * \section install_sec Instalação
 * Para instalar, basta utilizar compilar o arquivo através do comando <b>gcc main.c packagerLogger/packagerLogger.c encrypter/xorEncrypter.c</b>
 *
 * \section s Comandos aceitos
 * \subsection s1 Empacotando arquivos
 * <b>arquivoexecutavel nomepacote.arc caminho/para/arquivo1.extensão caminho/para/arquivo2.extensão</b>
 * <p>Caso o pacote não exista, um novo será criado</p>
 * \subsection s2 Extraindo arquivos
 * <b>arquivoexecutavel nomepacote.arc -ex nomearquivoempacotado.extensão</b>
 *
 * \subsection s3 Deletando logicamente
 * <b>arquivoexecutavel nomepacote.arc -rm nomearquivoempacotado.extensão</b>
 *
 * \subsection s4 Desfragmentando pacote.
 * <b>arquivoexecutavel nomepacote.arc -df</b>
 *
 * \subsection s5 Listando arquivos
 * <b>arquivoexecutavel nomepacote.arc -l</b>
 * \subsection s6 Testando Checksum
 * <b>arquivoexecutavel nomepacote.arc -tcs</b>
 * \subsection s7 Mostrando versão do empacotador
 * <b>nomeexecutavel -v</b>
 * \section a Bugs conhecidos
 * Encriptação XOR não funciona e a desfragmentação funciona corretamente, entretanto ela deixa um "rastro" no fim do arquivo e uma próxima desfragmentação não será possível. Além disso, não foi possível implementar 
 * o uso de LOGs para a atividade bônus.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include "packagerLogger/packagerLogger.h"
#include "encrypter/xorEncrypter.h"

#define PACKAGER_VERSION 1
#define SIGNATURE "SO"		
#define ENCRYPT_KEY "amoebabiiirkushbunny"
#define KEY_SIZE 20


#define ERR_PARAM_INVALID 2
#define ERR_FILE_NOT_FOUND 1
#define NO_ERROR 0		

#define PACKAGE_HEADER_SIZE 18l	


typedef struct fileHeader_s{
	unsigned long nextFileOffset;
	char isValid;
	char *name;
	unsigned long long headerOffsetFromBeginning;
}fileHeader;

typedef struct packagerHeader_s{ 
	char signature[3];	// SO\0
	unsigned int packagerVersion;
	unsigned long long checksum;
	unsigned int numFiles;
	FILE *arcFile;
}packagerHeader;


/**
 * @return key used for xor encryption for this packager version
 */
char* getKey();

/**
 * @return 0 if this current machine is big endian, 1 otherwise. To convert a big endian value to little endian value, use {@link #swapBytes(void *, unsigned int)}
 */
char isLittleEndian(); 

/**
 * Swap the bytes for the given address
 * @param a Start bytes address to swap.
 * @param size Type size
 * @see #isLittleEndian()
 */
void swapBytes(void *a, unsigned int size);

/**
 * Searches a file inside the package.
 * @param ph Package that possibly contain the desired file
 * @param name File name to be searched;
 * @return File header if found, NULL whenever ph or name is null or if file was not found. 
 */
fileHeader* findFileInPackageByName(packagerHeader *ph, char name[]);

/**
 * Returns file header from the given index. The arc file stream position prior to function call is restored before returning
 * @param ph Package informations. If null, function will return null immediately.
 * @param index Index of desired file. Must be lesser than the value in {@link packagerHeader#numFiles} or NULL is returned
 * @return File informations.
 */
fileHeader* getFileHeader(packagerHeader *ph, unsigned int index);

/**
 * Loads a package for the given name.
 * @param packageName package name to be searched and loaded.
 * @return Null if not found, header with package informations otherwise.
 */
packagerHeader* loadPackage(char *packageName);

/**
 * Creates a new package file, writing the basic information into the file.
 * @param packageName Name for the new file
 * @return Info header or NULL whenever param is null or memory allocation fails due to system out of memory.
 */
packagerHeader* newPackage(char *packageName);
 
 /**
  * Write 0 over the packaged file content, setting isInvalid flag inside fileHeader to 1.
  * @param ph packager header that contains the file to be invalidated. Cannot be null.
  * @param fileName File name to be invalidated.
  */
void invalidateFile(packagerHeader *ph, char *fileName);

/**
 * Searches for invalid packaged files and remove it from the package, as well as decrement the numFiles field from package header for each invalid files found.
 * @param ph Package header containing the file to defrag
 */
void defragPackage(packagerHeader *ph);

/** 
 * Prints all file names inside the package.
 * @param ph Header containing necessary data to access and print all the files.
 */
void showFiles(packagerHeader *ph);

/**
 * Appends the file to the package. Updates numFiles package field in file.
 * @param ph Header containing necessary data to append the file.
 * @param s Path to file.
 * @return 0 if no error was found, 2 or 1 if the file was not found or if any param are invalid.
 */
char packageFile(packagerHeader *ph, char *s);

/**
 * Package all desired files. Close the package file before returning.
 * @param ph Header containing all data needed to package the files. Cannot be null.
 * @param fileNames All file names to be packaged.
 * @param numFiles Number of files to be packaged.
 * @return Error code if found, 0 for no errors.
 */
char startPackaging(packagerHeader *ph, char *fileNames[], unsigned int numFiles);

/**
 * Generates the checksum number. This function will sum all the bytes found from the beginning of the first fileHeader. Writes the result into the package, in checksum field.
 * @param ph Header containing package file to generate the checksum.
 * @return Sum result.
 */ 
unsigned long long generateChecksum(packagerHeader *ph);

/**
 * Test if the checksum matches the checksum found inside the package file.
 * @param ph Header containing the file to be tested.
 * @return 1 if the checksum matches the number found on file, 0 otherwise.
 */
char testChecksum(packagerHeader *ph);

/**
 * Prints the version of the current packager.
 */
void printVersion();

/**
 *
 */
void redoFromLog(FILE *fp);

/**
 * Retrieve the file size in bytes. The file stream position prior to function call is restored before returning.
 * @param fp File to retrieve its size.
 * @return File size in bytes or -1 for error in ftell function.
 */
long fileSize(FILE *fp);

/**
 * 
 * @param s File name to check if exists.
 * @returns 0 if file does not exists or parameter is NULL.
 */
char fileExists(char *s);

/**
 * Extract the given file name from the arc file. If the file does not exists inside the package or the file already exists in the same folder the program runs,
 * a notification will be shown accordingly. This function will restore the arc file stream position before exit.
 * @param ph Header containing the package file to extract the given file.
 * @param fileName File name to be found.
 */
void extractFileFromPackage(packagerHeader *ph, char *fileName);

/**
 * @param fh File header to be measured
 * @returns Header size in bytes.
 */
unsigned int getFileHeaderSize(fileHeader *fh);

/**
 * Eliminates the given num of bytes from the end of file. File must be open for writing. This function does not check for null bytes, all given bytes are removed from the file
 * and the data is lost.
 * @param fp File to be reduced
 * @param bytesToReduce Num of bytes to be reduced.
 */
void reduceFileSize(FILE *fp, unsigned long long bytesToReduce);

/**
 * Stores the checksum into arc file. This function restore the stream position prior to function call.
 * @param ph Header containing arc file
 * @param checksum value to be stored
 */
void storeChecksum(packagerHeader *ph, unsigned long long checksum);

/**
 * Pointer to log file, used to register all operations made for this software.
 */
FILE *GLogFile = NULL;


int main(int argc, char *argv[]){
	
	if(argc <= 1){
		puts("type -help to see the valid commands");
		return 0;
	}
	
	if(!logExists()){
		GLogFile = createLogFile(argc, argv);
		assert(GLogFile !=  NULL);
	}
	
	unsigned int i;
	unsigned int len = strlen(argv[1]);
	packagerHeader *ph = NULL;
	
	if(len >= 5 && strcmp(".arc", &argv[1][len - 4]) == 0){
		
		if(argc < 3)
			puts("Type -help for more information");
		
		
		else if(strcmp(argv[2], "-l") == 0){
			showFiles(loadPackage(argv[1]));
		}
		
		else if(strcmp(argv[2], "-tcs") == 0){
			//test checksum
			ph = loadPackage(argv[1]);
			if(testChecksum(ph) != 0){
				printf("Checksum ok\n");
			}
			
			else{
				printf("Checksum does not match, arc file may be corrupted");
			}
			
		}
		
		else if(strcmp(argv[2], "-df") == 0){
			//defrag
			ph = loadPackage(argv[1]);
			defragPackage(ph);
		}
		
		else if(strcmp(argv[2], "-rm") == 0){
			//remove files
			i = 3;
			ph = loadPackage(argv[1]);
			
			while(i < argc){
				invalidateFile(ph, argv[i++]);
			}
			
		}
		
		else if(strcmp(argv[2], "-ex") == 0){
			//extract files
			i = 3;
			ph = loadPackage(argv[1]);
			
			while(i < argc){
				extractFileFromPackage(ph, argv[i++]);
			}
			
		}
		
		else if(strcmp(argv[2], "-ct") == 0){
			//test for corruption, use logs here
		}
		
		else{		
			packagerHeader *ph = loadPackage(argv[1]);
			if(ph == NULL) ph = newPackage(argv[1]);
			startPackaging(ph, &argv[2], argc - 2);
		}
		
	}	
	
	else if(strcmp("-v", argv[1]) == 0){
		printVersion();
	}
	
	if(ph != NULL) storeChecksum(ph, generateChecksum(ph));
	
	return 0;
}

char isLittleEndian(){
	
	int x = 1;
	char *c = (char *)&x;
	
	return c[0] == 1;
	
}

void swapBytes(void *a, unsigned int size){
	
	if(a == NULL || size == 0 || size == 1) return;
	
	int i = 0;
	int j = size - 1;
	size /= 2;
	
	char aux;
	
	while(i < size){
		aux = * (char *) (a + i);
		* (char *) (a + i) = * (char *) (a + j);
		* (char *) (a + j) = aux;
		
		i++;
		j--;
	}
	
}

void printVersion(){
	printf("ARC Packager version: %d\n", PACKAGER_VERSION);
}

void showFiles(packagerHeader *ph){
	if(ph == NULL) return;
	
	int i = 0;
	long startFileHeader;
	unsigned long nextFileHeaderOffset = 0;
	
	char isValid;
	char currentChar;
	
	fseek(ph -> arcFile, PACKAGE_HEADER_SIZE, SEEK_SET); // skip packageHeader
	
	while(i < ph -> numFiles && !feof(ph -> arcFile)){
		
		startFileHeader = ftell(ph -> arcFile);
		
		fread(&nextFileHeaderOffset, sizeof(nextFileHeaderOffset), 1, ph -> arcFile);
		if(feof(ph -> arcFile)) return;
		
		fread(&isValid, sizeof(char), 1, ph -> arcFile);
		if(feof(ph -> arcFile)) return;
		
		while(!feof(ph -> arcFile)){ // print file name
			fread(&currentChar, sizeof(char), 1, ph -> arcFile);
			
			if(currentChar == '\0'){
				if(isValid) printf("\n");
				break;
			}
			
			if(isValid) printf("%c", currentChar);
			
		}
		
		if(!isLittleEndian()) swapBytes(&nextFileHeaderOffset, sizeof nextFileHeaderOffset);
		fseek(ph -> arcFile, nextFileHeaderOffset + (unsigned long) startFileHeader, SEEK_SET); // seek to next file header
		
		i++;
	}
	
	fclose(ph -> arcFile);
}

packagerHeader* loadPackage(char *packageName){
	
	if(packageName == NULL) return NULL;
	
	FILE *fp = fopen(packageName, "rb+");
	
	if(fp == NULL) return NULL;
	
	packagerHeader *ph = malloc(sizeof(*ph));
	assert(ph != NULL);
	
	fread(ph -> signature, sizeof(char), 2, fp);
	if(feof(fp)){
		free(ph);
		return NULL;
	}
	
	ph -> signature[2] = '\0';
	ph -> arcFile = fp;
	
	fread(&ph -> packagerVersion, sizeof (ph -> packagerVersion), 1, fp);	
	
	if(strcmp(ph -> signature, SIGNATURE) != 0 || feof(fp)){
		free(ph);
		return NULL;
	}
	
	fread(&ph -> checksum, sizeof (ph -> checksum), 1, fp);
	if(feof(fp)){
		free(ph);
		return NULL;
	}
	
	fread(&ph -> numFiles, sizeof (ph -> numFiles), 1, fp);
	if(feof(fp)){
		free(ph);
		return NULL;
	}
	
	if(!isLittleEndian()){
		swapBytes(&ph -> packagerVersion, sizeof (ph -> packagerVersion));
		swapBytes(&ph -> checksum, sizeof (ph -> checksum));
		swapBytes(&ph -> numFiles, sizeof (ph -> numFiles));
	} 
	
	return ph;;
}

packagerHeader* newPackage(char *packageName){

	if(packageName == NULL) return NULL;
	
	FILE *fp = fopen(packageName, "wb+");
	if(fp == NULL){
		printf("Could not open file with name %s\n", packageName);
		return NULL;
	}
	
	packagerHeader *ph = malloc( sizeof(*ph) );
	if(ph == NULL) return NULL;
	
	strcpy(ph -> signature, SIGNATURE);
	ph -> packagerVersion = PACKAGER_VERSION;
	ph -> numFiles = 0;
	ph -> checksum = 0ll;
	ph -> arcFile = fp;
	
	int littleEndianPackagerVersion = ph -> packagerVersion;
	if(!isLittleEndian()) swapBytes(&littleEndianPackagerVersion, sizeof littleEndianPackagerVersion);
	
	fwrite(&ph -> signature, sizeof (char), 2, ph -> arcFile);
	fwrite(&littleEndianPackagerVersion, sizeof (littleEndianPackagerVersion), 1, ph -> arcFile);
	fwrite(&ph -> checksum, sizeof (ph -> checksum), 1, ph -> arcFile);
	fwrite(&ph -> numFiles, sizeof (ph -> numFiles), 1, ph -> arcFile);
	
	return ph;
}


char startPackaging(packagerHeader *ph, char *fileNames[], unsigned int numFiles){
	if(ph == NULL || fileNames == NULL) return ERR_PARAM_INVALID;
	
	
	int i;
	for(i = 0; i < numFiles; i++)
		packageFile(ph, fileNames[i]);
	
	registerLogTaskFinished(GLogFile);
	fclose(ph -> arcFile);
	
}


char packageFile(packagerHeader *ph, char *s){
	if(ph == NULL || s == NULL){
		return ERR_PARAM_INVALID;
	}
	
	FILE *fp = fopen(s, "rb");
	
	if(fp == NULL){
		printf("Could not open %s, file not found", s);
		return ERR_FILE_NOT_FOUND;
	}
	
	unsigned int i;
	unsigned int blockStep = 500;
	unsigned long currentByte = 0ll;
	
	char aux;
	char buffer[blockStep];
	
	long fileLen;
	long currentStreamPosition = ftell(ph -> arcFile); // saves current stream position
	long nextFileOffsetStreamPosition;
	
	fseek(ph -> arcFile, 0l, SEEK_END); // stream to the end of file
	nextFileOffsetStreamPosition = ftell(ph -> arcFile);
	
	if(!isLittleEndian()) swapBytes(&fileLen, sizeof fileLen);
	
	fwrite(&fileLen, sizeof(char), sizeof(fileLen), ph -> arcFile); /* fileHeader */
	aux = 1;
	fwrite(&aux, sizeof(char), 1, ph -> arcFile);
	fwrite(s, sizeof(char), strlen(s), ph -> arcFile);
	aux = '\0';
	fwrite(&aux, sizeof(char), 1, ph -> arcFile);	
	
	registerLogOpPackagingFile(GLogFile,s);
	
	while(!feof(fp)){
		registerLogCopyingBlock(GLogFile, currentByte, currentByte + (unsigned long) blockStep);
		i = fread(buffer, sizeof(char), blockStep, fp);
		//xorString(s, i, getKey());
		fwrite(buffer, sizeof(char), i, ph -> arcFile); // write buffer into arc file
		registerLogBlockCommited(GLogFile);
		currentByte += (unsigned long) i;
		
	}

	currentByte += sizeof(currentByte) + sizeof(char) + strlen(s) + sizeof(char); // add fileHeader space
	fseek(ph -> arcFile, nextFileOffsetStreamPosition, SEEK_SET); // prepare to update nextFileOffsetStreamPosition
	if(!isLittleEndian()) swapBytes(&currentByte, sizeof currentByte);
	fwrite(&currentByte, sizeof(char), sizeof(currentByte), ph -> arcFile);
	fclose(fp);
	
	ph -> numFiles++;
	i = ph -> numFiles;
	if(!isLittleEndian()) swapBytes(&i, sizeof i);
	
	fseek(ph -> arcFile, 14l, SEEK_SET);
	fwrite(&i, sizeof(i), 1, ph -> arcFile);	/* updates the numFiles info*/
	
	registerLogOpDone(GLogFile);
	
	fseek(ph -> arcFile, currentStreamPosition, SEEK_SET); // restore previous stream position
	
	return NO_ERROR;
	
}

long fileSize(FILE *fp){
	
	if(fp == NULL) return -1;
	long currentPosition = ftell(fp); // save the current position
	long size;
	
	fseek(fp, 0l, SEEK_END);
	size = ftell(fp); 
	
	fseek(fp, currentPosition, SEEK_SET); // restore previous position
	
	return size;
}

char fileExists(char *s){
	if(s == NULL) return 0;
	struct stat buffer;
	return stat(s, &buffer) == 0;
}

void extractFileFromPackage(packagerHeader *ph, char *fileName){
	if(ph == NULL || fileName == NULL) return;
	if(ph -> arcFile == NULL) return;

	fileHeader *fh = findFileInPackageByName(ph, fileName);

	if(fh == NULL){
		printf("File %s was not found in package\n", fileName);
		return;
	}
	
	if(!fh -> isValid){
		printf("File is not valid, logical delete was made, cannot extract %s\n", fh -> name);
		return;
	}
	
	if(fileExists(fileName)){
		printf("File %s already exists, considering rename or move it to another folder\nExtraction aborted...\n", fileName);
		return;
	}
	
	FILE *fp = fopen(fileName, "wb+"); // outputFile
	if(fp == NULL){
		printf("Error in fopen for file %s", fileName);
		return;
	}
	
	FILE *arcFile = ph -> arcFile;
	
	unsigned int i;
	unsigned int blockStep = 500;
	unsigned int fileHeaderSize =  getFileHeaderSize(fh); /* null terminate */
	
		
	unsigned long long current = 0ll;
	unsigned long sizeOfFileToExtract = fh -> nextFileOffset - fileHeaderSize;
	char buffer[blockStep];
	
	long currentStreamPosition = ftell(arcFile); // saves the stream position before function call
	
	fseek(arcFile, fh -> headerOffsetFromBeginning + fileHeaderSize, SEEK_SET); // position the stream to the beginning of the file content.
	
	registerLogOpExtractFile(GLogFile, fileName);
	
	while(current + (unsigned long long) blockStep < sizeOfFileToExtract && !feof(arcFile)){ // read packaged file as long as we can read a block of 500 bytes
		registerLogCopyingBlock(GLogFile, current, current + (unsigned long) blockStep);
		i = fread(buffer, sizeof (char), blockStep, arcFile); // read a block of 500 bytes from arc file
		current += i; 
		//xorString(buffer, i, getKey());
		fwrite(buffer, sizeof (char), blockStep, fp); // write the block read into output file
		registerLogBlockCommited(GLogFile);
	}
	
	if(sizeOfFileToExtract - current > 0){ // if we have bytes to extract
		registerLogCopyingBlock(GLogFile, current, sizeOfFileToExtract - current);
		fread(buffer, sizeof(char), sizeOfFileToExtract - current, arcFile);
		//xorString(buffer, i, getKey());
		fwrite(buffer, sizeof(char), sizeOfFileToExtract - current, fp);
		registerLogBlockCommited(GLogFile);
	}
	
	fclose(fp); // close output file
	registerLogOpDone(GLogFile);
	fseek(arcFile, currentStreamPosition, SEEK_SET); // restore the stream position before function call
	free(fh -> name);
	free(fh);
}

fileHeader* findFileInPackageByName(packagerHeader *ph, char name[]){
	
	if(ph == NULL || name == NULL) return NULL;
	
	unsigned int i = 0;
	fileHeader *fh = NULL;
	
	while(NULL != (fh = getFileHeader(ph, i++)) ){
		if(strcmp(name, fh -> name) == 0) break;
		
		free(fh -> name);
		free(fh); // discard unwanted file header
	}
	
	return fh;
	
}

/**
 * Returns file header from the given index. The arc file stream position prior to function call is restored before returning
 * @param ph Package informations. If null, function will return null immediately.
 * @param index Index of desired file. Must be lesser than the value in {@link packagerHeader#numFiles} or NULL is returned
 * @return File informations.
 */
fileHeader* getFileHeader(packagerHeader *ph, unsigned int index){
	
	if(ph == NULL || ph -> arcFile ==  NULL || index >= ph -> numFiles) return NULL;
	
	long streamPosition = ftell(ph -> arcFile);
	
	unsigned int i = 0;
	unsigned long nextFileHeaderOffset;
	
	fseek(ph -> arcFile, PACKAGE_HEADER_SIZE, SEEK_SET);
	
	while(i < index){ // iterate over the package file until we find the desired file
		
		fread(&nextFileHeaderOffset, sizeof nextFileHeaderOffset, 1, ph -> arcFile); // read where the next file header is
		
		if(!isLittleEndian()) swapBytes(&nextFileHeaderOffset, sizeof nextFileHeaderOffset);
		fseek(ph -> arcFile, nextFileHeaderOffset - sizeof nextFileHeaderOffset /* because last fread moved the stream */, SEEK_CUR); // go to the next file header
		
		i++;
	}
	
	fileHeader *fh = malloc(sizeof (*fh));
	assert(fh != NULL);
	fh -> headerOffsetFromBeginning = ftell(ph -> arcFile);
	
	fread(&fh -> nextFileOffset, sizeof(fh -> nextFileOffset), 1, ph -> arcFile);
	fread(&fh -> isValid, sizeof(fh -> isValid), 1, ph -> arcFile);
	
	if(!isLittleEndian()) swapBytes(&fh -> nextFileOffset, sizeof fh -> nextFileOffset);
	
	char fileNameBuffer[500];
	i = 0;
	do{
		fread(fileNameBuffer + i, sizeof(char), 1, ph -> arcFile);
		assert(!feof(ph -> arcFile));
	}while(fileNameBuffer[i++] != '\0'); // store the file name in buffer
	
	fh -> name = malloc(strlen(fileNameBuffer) + 1); // +1 for \0
	assert(fh -> name != NULL);
	strcpy(fh -> name, fileNameBuffer);
	
	fseek(ph -> arcFile, streamPosition, SEEK_SET); // restore stream position prior to function call
	
	return fh;
	
}

void invalidateFile(packagerHeader *ph, char *fileName){
	
	if(ph ==  NULL || fileName == NULL) return;
	
	fileHeader *fh = findFileInPackageByName(ph, fileName);
	
	if(fh == NULL){
		printf("File %s not found in package\n", fileName);
	}
	
	unsigned int fileHeaderSize =  getFileHeaderSize(fh);
	
	unsigned int blockStep = 500;
	unsigned long long currentByte = 0ll;
	unsigned long long sizeOfFileToInvalidate = fh -> nextFileOffset - fileHeaderSize;
	
	char eraseValue = 0;
	
	unsigned long currentStreamPosition = ftell(ph -> arcFile); /* save current stream position prior to function call */
	
	fseek(ph -> arcFile, fh -> headerOffsetFromBeginning + sizeof (fh -> nextFileOffset) , SEEK_SET); /* position stream to the beginning of header file and skip nextFileOffset field*/
	registerLogOpEraseFile(GLogFile, fileName);
	fwrite(&eraseValue, sizeof eraseValue, 1, ph -> arcFile); /* write isValid = 0 in package */
	
	fseek(ph -> arcFile, fh -> headerOffsetFromBeginning + fileHeaderSize, SEEK_SET); /* position stream to the beginning of file content */
	
	while(currentByte < sizeOfFileToInvalidate && !feof(ph -> arcFile)){ /* write 0 over all file content */
			if(currentByte % blockStep == 0) registerLogErasingBlock(GLogFile, currentByte, currentByte + blockStep);
			fwrite(&eraseValue, sizeof(char), 1, ph -> arcFile);
			currentByte += 1;
			if(currentByte % blockStep == 0) registerLogBlockCommited(GLogFile);
	}
	
	registerLogOpDone(GLogFile);

	fseek(ph -> arcFile, currentStreamPosition, SEEK_SET); /* restores stream position */
	free(fh -> name);
	free(fh);
	
}

unsigned int getFileHeaderSize(fileHeader *fh){
	if(fh == NULL || fh -> name == NULL) return 0;
	return sizeof(fh -> isValid) + sizeof(fh -> nextFileOffset) + strlen(fh -> name) + 1 /* null terminate */;
}

void reduceFileSize(FILE *fp, unsigned long long bytesToReduce){
	if(fp == NULL) return;
	ftruncate(fileno(fp), bytesToReduce);
}

void defragPackage(packagerHeader *ph){
	if(ph == NULL) return;
	
	unsigned int i = 0;
	unsigned int numFilesAux;
	long currentPackageSize;
	long streamPositionToWrite;
	long streamPositionToRead;
	long streamPositionToContinue;
	unsigned long long bytesToPull;
	
	char buffer;
	
	fileHeader *fh = NULL;
	
	while(i < ph -> numFiles){
		fh = getFileHeader(ph, i);
		
		if(fh -> isValid){
			free(fh -> name);
			free(fh);
			i++;
			continue;
		}
		
		streamPositionToWrite = fh -> headerOffsetFromBeginning;
		streamPositionToContinue = streamPositionToWrite; /* save stream position to return after removing invalid file */
		bytesToPull = 0ll;
		
		bytesToPull = fh -> nextFileOffset;
		
		fseek(ph -> arcFile, bytesToPull + fh -> headerOffsetFromBeginning, SEEK_SET); /* skip invalid file content */
		streamPositionToRead = ftell(ph -> arcFile);
		
		currentPackageSize = fileSize(ph -> arcFile);
		
		while(streamPositionToRead <= currentPackageSize){
			fread(&buffer, sizeof(buffer), 1, ph -> arcFile);
			streamPositionToRead++;
			fseek(ph -> arcFile, streamPositionToWrite, SEEK_SET);
			fwrite(&buffer, sizeof(buffer), 1, ph -> arcFile);
			streamPositionToWrite++;
			fseek(ph -> arcFile, streamPositionToRead, SEEK_SET);
		}
		
		fseek(ph -> arcFile, 2 /* signature */ + sizeof(ph -> packagerVersion) + sizeof(ph -> checksum), SEEK_SET); /* seek to numFiles field */
		ph -> numFiles--;
		numFilesAux = ph -> numFiles;
		if(!isLittleEndian()) swapBytes(&numFilesAux, sizeof(numFilesAux));
		fwrite(&numFilesAux, sizeof(numFilesAux), 1, ph -> arcFile); /* update numFiles in package */
		
		reduceFileSize(ph -> arcFile, bytesToPull);
		storeChecksum(ph, generateChecksum(ph));
		
		fseek(ph -> arcFile, streamPositionToContinue, SEEK_SET); /* continue from where we stopped */
		free(fh -> name);
		free(fh);
		
	}
	
	
}

void storeChecksum(packagerHeader *ph, unsigned long long checksum){
	
	if(ph == NULL) return;
	
	long streamPosition = ftell(ph -> arcFile);
	
	fseek(ph -> arcFile, 2 /* signature */ + sizeof (ph -> packagerVersion), SEEK_SET);
	
	if(!isLittleEndian()) swapBytes(&checksum, sizeof checksum);
	fwrite(&checksum, sizeof checksum, 1, ph -> arcFile);
	
	fseek(ph -> arcFile, streamPosition, SEEK_SET); /* restore stream position prior to function call */
	
}

unsigned long long generateChecksum(packagerHeader *ph){
	
	if(ph == NULL) return;
	
	fseek(ph -> arcFile, PACKAGE_HEADER_SIZE, SEEK_SET);
	
	char buffer;
	unsigned long long checksum = 0l;

	while(!feof(ph -> arcFile)){
		fread(&buffer, sizeof buffer, 1, ph -> arcFile);
		checksum += (unsigned long long) buffer;
	}
	
	return checksum;
}

char testChecksum(packagerHeader *ph){
	if(ph == NULL) return 0;
	
	unsigned long long checksum = generateChecksum(ph);
	return checksum == ph -> checksum;
}

char* getKey(){
	return ENCRYPT_KEY;
}