
/*
 * All writing operations consider the current file stream position.
 */
 
#ifndef PACKAGER_LOGGER_INCLUDED
	
	#define PACKAGER_LOGGER_INCLUDED
	#include<stdio.h>	
	#include<sys/types.h>
	#include<sys/stat.h>
	#include<unistd.h>
	
	/**
	 * Logger version for possible backward compatibility.
	 */
	#define LOGGER_VERSION 1
	
	/**
	 * Used when a block is about to be copied and encrypted. Ex: cb <xxx> <yyy> where xxx is the byte 
	 * where the block begins and yyy where the block ends. Both are long long
	 * @see #REGISTER_BLOCK_COMMITTED
	 */
	#define REGISTER_COPYING_BLOCK "cb"
	
	/**
	 * Used when a given block was committed, i.e., moved or encrypted and saved in arc file.
	 */
	#define REGISTER_BLOCK_COMMITED "bc"
	
	/**
	 * Used when a given block have to be erased, writing zeros over it. Ex: eb <xxx> <yyy> where xxx is the byte 
	 * where the block begins and yyy where the block ends. Both are long long
	 */
	#define REGISTER_ERASING_BLOCK "eb"
	
	/**
	 * First log line. Ex: ts <input_from_command_line>
	 */
	#define REGISTER_TASK_STARTED "ts"
	
	/**
	 * Used when the task that created the log file was finished
	 */
	#define REGISTER_TASK_FINISHED "tf"
	
	/**
	 * Used when erase operation was requested, zeros will be wrote over the file content. Ex: ef <file name>
	 */
	#define REGISTER_OP_ERASE_FILE "ef"
	/**
	 * Used when a file is going to be packaged. Ex: pf <file_name>
	 */
	#define REGISTER_OP_PACKAGE_FILE "pf"
	
	/**
	 * Used when an invalid file was found in .arc and defrag is about to begin. Ex: ds <file name>
	 */
	#define REGISTER_OP_DEFRAG_STARTED "ds"
	
	/**
	 * Used when an extract was requested and it is about to start. Example: es <file name>
	 */
	#define REGISTER_OP_EXTRACT_STARTED "es"
	
	/**
	 * Used when a operation was finished.
	 */
	#define REGISTER_OP_DONE "od"
	
	/**
	 * Default name for log file.
	 */
	#define DEFAULT_LOG_NAME "arclog.pl"
	


	/**
	 * Checks if {@link #REGISTER_TASK_FINISHED} is present at the final of the log file.
	 * @param fp
	 */
	char isLogTaskFinished(FILE *fp);

	/**
	 * Reads the first line from lo file and store the user input in buffer. File stream position is restored before function ends.
	 * @param fp Log file. Cannot be null
	 * @param buffer Buffer to receive the input.
	 * @param bufferSize Size of buffer, must have enough len to receive all the input from command line.
	 */
	void getLogTask(FILE *fp, char *buffer, unsigned int bufferSize);

	/**
	 * Check if a log file exists
	 * @return 1 if exists, 0 otherwise.
	 */
	char logExists();

	/**
	 * Creates a new log file with default name {@link #DEFAULT_LOG_NAME}. The new file will contain {@link #LOGGER_VERSION} in first line and
	 * {@link #REGISTER_TASK_STARTED} plus the value received in the param in second line.
	 * @param argv Input typed by the user to the program.
	 * @param argc Num of arguments received from the user.
	 * @return Pointer to file (w+) or null if was not possible to create the log.
	 */
	FILE* createLogFile(int argc, char *argv[]);

	/**
	 * <p>Delete the log file. This function first checks the current logger version, log file must have the default name {@link #DEFAULT_LOG_NAME} 
	 * (may vary depending on {@link #LOGGER_VERSION}))</p>
	 * <p>This function closes the file before removing</p>
	 * @param fp File to be deleted. Must not be closed before function call
	 */
	void deleteLogFile(FILE *fp);

	/**
	 * Register all operations was finished successfully, writing {@link #REGISTER_TASK_FINISHED}
	 * @param fp File log.
	 */
	void registerLogTaskFinished(FILE *fp);

	/**
	 * Write {@link #REGISTER_COPYING_BLOCK } plus the remaining param values.
	 * @param fp Log file.
	 * @param start Byte where the block begins
	 * @param end Byte where the block ends.
	 */
	void registerLogCopyingBlock(FILE *fp, unsigned long long start, unsigned long long end);

	/**
	 * Register {@link #REGISTER_ERASING_BLOCK} followed by the two remaining parameters.
	 * @param fp Log file.
	 * @param start Byte where the block begins
	 * @param end Byte where the block ends. 
	 */
	void registerLogErasingBlock(FILE *fp, unsigned long long start, unsigned long long end);

	/**
	 * Register {@link #REGISTER_OP_PACKAGE_FILE} followed by the file name to be packaged.
	 * @param fp Log file.
	 * @param fileName File name that will be packaged.
	 */
	void registerLogOpPackagingFile(FILE *fp, char *fileName);

	/**
	 * Register {@link #REGISTER_OP_ERASE_FILE} followed by the file name that will be erased in package.
	 * @param fp Log file.
	 * @param fileName File name that will be erased (write zero over its content).
	 */
	void registerLogOpEraseFile(FILE *fp, char *fileName);

	/**
	 * Register {@link #REGISTER_OP_DEFRAG_STARTED}.
	 * @param fp Log file.
	 */
	void registerLogOpDefragPackage(FILE *fp);

	/**
	 * Register {@link #REGISTER_OP_EXTRACT_STARTED}.
	 * @param fp Log file.
	 * @param fileName File that will be extracted.
	 */
	void registerLogOpExtractFile(FILE *fp, char *fileName);

	/**
	 * Write {@link #REGISTER_BLOCK_COMMITED}.
	 * @param fp Log file.
	 */
	void registerLogBlockCommited(FILE *fp);

	/**
	 * Register {@link #REGISTER_OP_DONE}.
	 * @param fp Log file.
	 */
	void registerLogOpDone(FILE *fp);

	/**
	 * @return Current packager logger version
	 */
	unsigned int getLoggerVersion();


	/**
	 * @param fp Log file.
	 * @param buffer Buffer to receive the line content. Buffer will receive the entire line (with line break \n plus null to terminate the buffer).
	 * @param bufferSize Size of buffer to store the line content.
	 * @return 0 if the some parameter is invalid or -1 if EOF was reached before buffer write.
	 */
	char readLog(FILE *fp, char *buffer, int bufferSize);

#endif