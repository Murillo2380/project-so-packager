
#include "packagerLogger.h"

/*
 * All writing operations consider the current file stream position.
 */

/**
 * Checks if {@link #REGISTER_TASK_FINISHED} is present at the final of the log file.
 * @param fp
 */
char isLogTaskFinished(FILE *fp){
	
	return 0;
}

/**
 * Reads the first line from lo file and store the user input in buffer. File stream position is restored before function ends.
 * @param fp Log file. Cannot be null
 * @param buffer Buffer to receive the input.
 * @param bufferSize Size of buffer, must have enough len to receive all the input from command line.
 */
void getLogTask(FILE *fp, char *buffer, unsigned int bufferSize){
	if(fp == NULL || buffer == NULL) return;
	long streamPosition = ftell(fp);
	
	rewind(fp);
	fscanf(fp, "%s%s%s");
	
	fseek(fp, streamPosition, SEEK_SET);
	
}

/**
 * Check if a log file exists
 * @return 1 if exists, 0 otherwise.
 */
char logExists(){
	
	struct stat buffer;
	return stat(DEFAULT_LOG_NAME, &buffer) == 0;
	
}

/**
 * Creates a new log file with default name {@link #DEFAULT_LOG_NAME}. The new file will contain {@link #LOGGER_VERSION} in first line and
 * {@link #REGISTER_TASK_STARTED} plus the value received in the param in second line.
 * @param argv Input typed by the user to the program.
 * @param argc Num of arguments received from the user.
 * @return Pointer to file (w+) or null if was not possible to create the log.
 */
FILE* createLogFile(int argc, char *argv[]){
	
	if(argv == NULL || argc <= 0) return NULL;
	
	FILE *fp = fopen(DEFAULT_LOG_NAME, "w+");
	if(fp == NULL) return fp;
	
	int i = 0;
	fprintf(fp, "%u\n" REGISTER_TASK_STARTED, LOGGER_VERSION);
	
	while(i < argc){
		if(i + 1 == argc) fprintf(fp, " %s\n", argv[i]);
		else fprintf(fp, " %s", argv[i]);
		i++;
	}
	
	return fp;
	
}

/**
 * <p>Delete the log file. This function first checks the current logger version, log file must have the default name {@link #DEFAULT_LOG_NAME} 
 * (may vary depending on {@link #LOGGER_VERSION}))</p>
 * <p>This function closes the file before removing</p>
 * @param fp File to be deleted. Must not be closed before function call
 */
void deleteLogFile(FILE *fp){
	if(fp == NULL) return;
	rewind(fp);
	
	int loggerVersion;
	fscanf(fp, "%d", &loggerVersion);
	
	if(loggerVersion == 1) {
		fclose(fp);
		remove(DEFAULT_LOG_NAME);
	}
	
} 

/**
 * Register all operations was finished successfully, writing {@link #REGISTER_TASK_FINISHED}
 * @param fp File log.
 */
void registerLogTaskFinished(FILE *fp){
	if(fp == NULL) return;
	fprintf(fp, REGISTER_TASK_FINISHED "\n");
}

/**
 * Write {@link #REGISTER_COPYING_BLOCK } plus the remaining param values.
 * @param fp Log file.
 * @param start Byte where the block begins
 * @param end Byte where the block ends.
 */
void registerLogCopyingBlock(FILE *fp, unsigned long long start, unsigned long long end){
	if(fp == NULL) return;
	fprintf(fp, REGISTER_COPYING_BLOCK " %llu %llu\n", start, end);
}

/**
 * Register {@link #REGISTER_ERASING_BLOCK} followed by the two remaining parameters.
 * @param fp Log file.
 * @param start Byte where the block begins
 * @param end Byte where the block ends. 
 */
void registerLogErasingBlock(FILE *fp, unsigned long long start, unsigned long long end){
	if(fp == NULL) return;
	fprintf(fp, REGISTER_ERASING_BLOCK "%llu %llu\n", start, end);
}

/**
 * Register {@link #REGISTER_OP_PACKAGE_FILE} followed by the file name to be packaged.
 * @param fp Log file.
 * @param fileName File name that will be packaged.
 */
void registerLogOpPackagingFile(FILE *fp, char *fileName){
	if(fp == NULL || fileName == NULL) return;
	
	fprintf(fp, REGISTER_OP_PACKAGE_FILE " %s\n", fileName);	
}

/**
 * Register {@link #REGISTER_OP_ERASE_FILE} followed by the file name that will be erased in package.
 * @param fp Log file.
 * @param fileName File name that will be erased (write zero over its content).
 */
void registerLogOpEraseFile(FILE *fp, char *fileName){
	if(fp == NULL || fileName == NULL) return;
	fprintf(fp, REGISTER_OP_ERASE_FILE " %s\n", fileName);
}

/**
 * Register {@link #REGISTER_OP_DEFRAG_STARTED}.
 * @param fp Log file.
 */
void registerLogOpDefragPackage(FILE *fp){
	if(fp == NULL) return;
	fprintf(fp, REGISTER_OP_DEFRAG_STARTED "\n");
	
}

/**
 * Register {@link #REGISTER_OP_EXTRACT_STARTED}.
 * @param fp Log file.
 * @param fileName File that will be extracted.
 */
void registerLogOpExtractFile(FILE *fp, char *fileName){
	if(fp == NULL || fileName == NULL) return;
	fprintf(fp, REGISTER_OP_EXTRACT_STARTED " %s\n", fileName);
}

/**
 * Write {@link #REGISTER_BLOCK_COMMITED}.
 * @param fp Log file.
 */
void registerLogBlockCommited(FILE *fp){
	if(fp == NULL) return;
	fprintf(fp, REGISTER_BLOCK_COMMITED "\n");
	
}

/**
 * Register {@link #REGISTER_OP_DONE}.
 * @param fp Log file.
 */
void registerLogOpDone(FILE *fp){
	if(fp == NULL) return;
	fprintf(fp, REGISTER_OP_DONE "\n");
}

/**
 * @return Current packager logger version
 */
unsigned int getLoggerVersion(){
	return LOGGER_VERSION;
}


/**
 * @param fp Log file.
 * @param buffer Buffer to receive the line content. Buffer will receive the entire line (with line break \n plus null to terminate the buffer).
 * @param bufferSize Size of buffer to store the line content.
 * @return 0 if the some parameter is invalid or -1 if EOF was reached before buffer write.
 */
char readLog(FILE *fp, char *buffer, int bufferSize){
	if(fp == NULL || buffer == NULL || bufferSize <= 0) return;
	if(fgets(buffer, bufferSize, fp) == NULL) return -1;
	return 0;
}