#include"xorEncrypter.h"

/**
 * Xor all the character on the given string for the given key. If size is greater than key length than xor will continue from the start of the key.
 * @param s String to xor
 * @param size Num of bytes to be affected. Must be lesser than s length.
 * @param key Key to be used for xor
 */
void xorString(char *s, unsigned int size, char *key){
	
	if(s ==  NULL || key == NULL) return;
	
	int i = 0;
	int j = 0;
	int keyLen = strlen(key);
	
	if(keyLen <= 0) return;
	
	while(i < size){
		s[i] = s[i] ^ key[j];
		j++;
		if(j == keyLen) j = 0;
		i++;
	}
	
}