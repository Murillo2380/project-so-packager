#ifndef SO_XOR_ENCRYPTER_INCLUDED
	#define SO_XOR_ENCRYPTER_INCLUDED
	#include <string.h>
#endif

/**
 * Xor all the character on the given string for the given key. If size is greater than key length than xor will continue from the start of the key.
 * @param s String to xor
 * @param size Num of bytes to be affected. Must be lesser than s length.
 * @param key Key to be used for xor
 */
void xorString(char *s, unsigned int size, char *key);